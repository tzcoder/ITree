package itree.demo;

import com.sise.itree.ITreeApplication;

/**
 * @author idea
 * @data 2019/4/30
 */
public class WebApplication {

    public static void main(String[] args) throws IllegalAccessException, InstantiationException {
        ITreeApplication.start(WebApplication.class);
    }
}
